from GeoCodingApiService import GeoCodingApiService

key = "b311be6279764ba38c9f01daf8726bb9"

def main():
    geoCodingApiService = GeoCodingApiService(key)
    placeName = input("Enter the place name: ")
    url = geoCodingApiService.getUrl(placeName)
    resonseObject = geoCodingApiService.getResponse(url)
    jsonObject = geoCodingApiService.getJsonObject(resonseObject)

    try:
        locationPoints = geoCodingApiService.getLongitudeLatitudePoints(jsonObject)
    except:
        print("This place does not exist..!!")
        exit(0)

    print("Latitude: ", locationPoints[0])
    print("Longitude: ", locationPoints[1])

main()