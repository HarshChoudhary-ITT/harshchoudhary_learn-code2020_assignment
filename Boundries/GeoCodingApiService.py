import requests
import json

class GeoCodingApiService:

    def __init__(self, key):
        self.key = key

    def getUrl(self, placeName):
        URL = "https://api.opencagedata.com/geocode/v1/json?q=" + placeName + "&key=" + self.key 
        return URL

    def getResponse(self, URL):
        return requests.get(URL)

    def getJsonObject(self, responseObject):
        return json.loads(responseObject.text)

    def getLongitudeLatitudePoints(self, jsonObject):
        locationPoints = []
        locationPoints.append(jsonObject['results'][0]['bounds']['northeast']['lat'])
        locationPoints.append(jsonObject['results'][0]['bounds']['northeast']['lng'])
        return locationPoints