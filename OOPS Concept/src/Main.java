
public class Main {

	public static void main(String[] args) {
		Mobile mobile= new Mobile("Nokia", "Win8", "Lumia", 10000);
		System.out.println(mobile.getModel());

		Android android = new Android("Samsung", "Android", "Grand", 30000);
		System.out.println(android.getModel());

		BlackBerry blackBerry = new BlackBerry("BlackB", "RIM", "Curve", 20000);
		System.out.println(blackBerry.getModel());

	}

}
