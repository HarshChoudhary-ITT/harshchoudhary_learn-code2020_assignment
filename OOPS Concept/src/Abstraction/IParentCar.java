package Abstraction;

public interface IParentCar {
	public void changeGear(int newValue);

	public void speedUp(int increment);

	public void applyBrakes(int decrement);
}
