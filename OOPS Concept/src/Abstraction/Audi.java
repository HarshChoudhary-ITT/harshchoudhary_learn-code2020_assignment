package Abstraction;

public class Audi implements IParentCar {
	int speed = 0;
	int gear = 1;

	public static void main(String[] args) {
		Audi audiA6 = new Audi();
		audiA6.speedUp(50);
		audiA6.printStates();
		audiA6.changeGear(4);
		audiA6.speedUp(100);
		audiA6.printStates();

	}

	@Override
	public void changeGear(int newValue) {
		gear=newValue;
	}

	@Override
	public void speedUp(int increment) {
		speed=speed+increment;

	}

	@Override
	public void applyBrakes(int decrement) {
		speed=speed-decrement;
	}

	void printStates() {
		System.out.println("speed : " + speed + "\tgear : " + gear);
	}

}
