
public class BlackBerry extends Mobile {
	
	BlackBerry(String manufacturer, String operationSystem, String model, int cost) {
		super(manufacturer, operationSystem, model, cost);
	}

	public String getModel() {
		return "This is Blackberry- " + model;
	}
}