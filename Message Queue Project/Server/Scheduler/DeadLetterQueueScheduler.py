import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import schedule 
import time 
from _datetime import datetime, timedelta
from Server.CustomExceptions.DeadLetterQueueSchedulerException import *
from Server.ServerConstant import *

class deadLetterScheduler:
    __queueObject = ""

    def initScheduler(self, queueObject):
        print(SCHEDULER_STARTED_MESSAGE)
        try:
            self.__queueObject = queueObject
            schedule.every(30).minutes.do(self.removeExpiryMessages)
            while True:
                schedule.run_pending() 
                time.sleep(1)
        except:
            raise DeadLetterQueueSchedulerException(DEAD_LETTER_EXCEPTION_MESSAGE)

    def removeExpiryMessages(self):
        if self.__queueObject: 
            for topic in self.__queueObject.data:
                for message in topic.messages:
                    if message.expireTime > datetime.now().strftime('%Y-%m-%d %H:%M:%S'):
                        pass
                    else:
                        topic.messages.remove(message)
            #print(self.__queueObject)
