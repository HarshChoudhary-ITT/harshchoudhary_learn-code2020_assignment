import sys
sys.path.append("D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import unittest
from unittest.mock import patch
import socket
from Server.serveService import serverService
from Server.Role.Publisher.PublisherService import PublisherService
from Server.Model.Queue import Queue

class Server_Test(unittest.TestCase):
    serverObject = serverService()
    serverSocket = socket.socket()

    def testIsInstanceOfServer(self):
        self.assertIsInstance(self.serverObject, serverService)

    def testServerSocketcreation(self):
        socketCreated = self.serverObject.createSocket()
        self.assertEqual(type(socketCreated), type(self.serverSocket))
        socketCreated.close()

    def testSocketBinding(self):
        self.assertIsNone(self.serverObject.bindSocket(self.serverSocket))

    def testSocketIsListening(self):
        self.assertIsNone(self.serverObject.setServerListener(self.serverSocket))

    def testHostIP(self):
        self.assertTrue(socket.gethostbyname('localhost'))
