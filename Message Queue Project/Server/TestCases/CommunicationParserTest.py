import sys
sys.path.append("D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import unittest
from unittest.mock import patch
from Server.Protocol.Parser import *
from Server.Protocol.Response import *
from Server.ServerConstant import *

class CommunicationParser_Test(unittest.TestCase):
    __parserObject = Parser()

    def testIsInstanceOfParser(self):
        self.assertIsInstance(self.__parserObject, Parser)

    def testSerializeToJsonConvertor(self):
        message = "message"
        responseObject = Response(message)
        response = self.__parserObject.serializeToJsonConvertor(responseObject)
        self.assertEqual(response, '"{\\"data\\": \\"message\\", \\"dataFormat\\": \\"json\\", \\"version\\": \\"1.0\\", \\"status\\": \\"Message sent\\"}"')
 
    def testDeserializeJsonFormatedData(self):
        message = "message"
        requestObject = Response(message)
        serializeResponse = self.__parserObject.serializeToJsonConvertor(requestObject)
        desrializeResponse = self.__parserObject.desrializeJsonFormattedData(serializeResponse)
        self.assertEqual(desrializeResponse, {'data': 'message', 'dataFormat': 'json', 'version': '1.0', 'status': 'Message sent'})
         