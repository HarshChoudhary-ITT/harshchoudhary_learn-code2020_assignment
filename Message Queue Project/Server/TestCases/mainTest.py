import os
import sys
path = os.path.abspath(__file__)
path = path + "/../../.."
sys.path.append(path)

from Server.TestCases.ServerTest import *
from Server.TestCases.CommunicationParserTest import *


if __name__ == '__main__':  
    unittest.main()