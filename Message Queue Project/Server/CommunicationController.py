import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.Protocol.Parser import *
from Server.Protocol.Response import *
from Server.ServerConstant import *

class CommunicationController:
    def sendMessageToClient(self, connection, message):
        responseObject = self.getResponseObject(message)
        responseJsonobject = Parser().serializeToJsonConvertor(responseObject)
        connection.sendall(str.encode(responseJsonobject))

    def receiveMessageFromClient(self, connection):
        data = connection.recv(1024)
        response = data.decode('utf-8')
        clientMessageInJson = Parser().desrializeJsonFormattedData(response)
        return clientMessageInJson

    def getResponseObject(self, data):
        responseObject = Response(data)
        return responseObject