import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.serveService import *
from Server.CustomExceptions.ServerSocketException import *
from Server.CustomExceptions.ServerListenerException import *
from Server.CustomExceptions.CreateSocketException import *
from Server.CustomExceptions.BindSocketException import *

def startServer():
    serverInstance = serverService()
    serverSocket = serverInstance.createSocket()
    try:
        if serverSocket is None:
            raise ServerSocketException(SERVER_SOCKET_EXCEPTION_MESSAGE)
        print(SOCKET_CREATED_MESSAGE)
        serverInstance.bindSocket(serverSocket)
        serverInstance.setServerListener(serverSocket)
        print(LISTENING_FOR_CONNECTION_MESSAGE)
        serverInstance.createDatabaseTables()
        serverInstance.initQueue()
        serverInstance.initDeadLetterQueueScheduler()
        serverInstance.initializeTopic()

        while True:
            clientConnection, IPAddress = serverInstance.acceptConnectionWithClient(
                serverSocket)
            print(CONNECTED_TO_MESSAGE + IPAddress[0] + COLON + str(IPAddress[1]))
            clientName = CLIENT + UNDERSCORE + str(IPAddress[1])
            try:
                start_new_thread(
                    serverInstance.start_new_client_thread, (clientConnection, clientName))
            except:
                print(SOMETHING_WENT_WRONG_MESSAGE)

        serverInstance.closeSocket(serverSocket)
        
    except (CreateSocketException, BindSocketException, ServerSocketException, ServerListenerException) as error:
        errorMessage = error.args[0]
        print(errorMessage)

    except Exception as error:
        print(UNABLE_TO_CONNECT_ERROR_MESSAGE + str(error))


startServer()
