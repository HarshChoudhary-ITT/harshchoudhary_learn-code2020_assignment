import sys
sys.path.append("D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.Database.DatabaseController import *


class TopicService: 
    __listOfTopics = []

    def initilalizeTopic(self):
        self.__listOfTopics = [('Morning Updates',),('Evening Updates',)]

    def getTopics(self):
        return self.__listOfTopics

    def connectWithDatabase(self):
        databaseController = DatabaseController()
        databaseController.initDatabaseConnection() 
        self.__addTopicsInDatabase(databaseController)

    def __addTopicsInDatabase(self, databaseController): 
        databaseController.addTopicDataIntoTable(self.__listOfTopics)
