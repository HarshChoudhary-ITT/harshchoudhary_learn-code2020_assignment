import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")

from Server.Topic.TopicService import *

class TopicController:
    __topicService = ""

    def initializeService(self):
        self.__topicService = TopicService()
        self.__topicService.initilalizeTopic()
        self.__topicService.connectWithDatabase()
    
    def getTopicsList(self):
        topicList = self.__topicService.getTopics()
        return topicList