HOST = '127.0.0.1'
PORT = 1234
EXIT = "exit"
PUBLISHER = 1

DEAD_LETTER_EXCEPTION_MESSAGE = "Something went wrong, not able to initiate scheduler"
SERVER_SOCKET_EXCEPTION_MESSAGE = "Socket is busy, not able to run server"
SERVER_LISTENER_EXCEPTION_MESSAGE = "Something went wrong, not able to set the listener"

SOCKET_CREATED_MESSAGE = "Server Socket successfully"
LISTENING_FOR_CONNECTION_MESSAGE = 'Listening for a connection...'
CONNECTED_TO_MESSAGE = 'Connected to: '
COLON = ":"
CLIENT = "client"
UNDERSCORE = "_"
SOMETHING_WENT_WRONG_MESSAGE = "Something went wrong, please try again later..."
UNABLE_TO_CONNECT_ERROR_MESSAGE = "Unable to connect with server :- "
CREATE_SOCKET_ERROR_MESSAGE = "Socket is busy, please use another socket"
BIND_SOCKET_ERROR_MESSAGE = "Please check the hostname and portnumber, unable to bind with socket"
DEAD_LETTER_NOT_WORKING_MESSAGE = "Something went wrong, dead letter scheduler is not working"
DATA = "data"
CONNECTION_CLOSED_MESSAGE = "connection closed successfully"
WELCOME_MESSAGE = "Welcome to the Message Queue Server"
CLIENT_ROLE_MESSAGE = "client Role " 
CLIENT_CHOOSE_TOPIC_MESSAGE = "client choose Topic "
SCHEDULER_STARTED_MESSAGE = "Scheduler Started"
MESSAGE_SENT = "Message sent"
POST = "POST"