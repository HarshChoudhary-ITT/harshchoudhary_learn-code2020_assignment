from dataclasses import dataclass
from Server.Protocol.IMQProtocol import *
from Server.ServerConstant import *

@dataclass
class Response(IMQProtocol):
    status: str = MESSAGE_SENT