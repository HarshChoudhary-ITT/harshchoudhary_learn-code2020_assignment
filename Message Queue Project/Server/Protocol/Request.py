from dataclasses import dataclass
from Server.Protocol.IMQProtocol import *
from Server.ServerConstant import *

@dataclass
class Request(IMQProtocol):
    requestType: str = POST
