from dataclasses import dataclass


@dataclass
class IMQProtocol:
    data: str
    dataFormat: str = "json"
    version: str = "1.0"
