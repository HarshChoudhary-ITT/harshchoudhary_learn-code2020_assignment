import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from dataclasses import *
from Server.Model.Topic import *

@dataclass
class Queue:
    data : list[Topic] =  field(default_factory=list)