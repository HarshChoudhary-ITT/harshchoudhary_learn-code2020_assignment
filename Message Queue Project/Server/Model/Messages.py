import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from dataclasses import *

@dataclass
class Messages:
    messageData : str 
    arrivalTime : str 
    expireTime : str 
    serialNo : int = 0