import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from dataclasses import *
from Server.Model.Messages import *

@dataclass
class Topic:
    topicId : int = 0
    messages : list[Messages] =  field(default_factory=list)