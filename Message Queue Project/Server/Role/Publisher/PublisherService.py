import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.CommunicationController import *
from Server.Database.DatabaseController import *
from Server.Model.Queue import *
from Server.Model.Messages import *
from _datetime import datetime, timedelta

class PublisherService:

    __clientConnection = ""
    __clientName = ""
    __topicID = 0
    __databaseController = DatabaseController()
    __EXIT = "exit"
    __communicationController = CommunicationController()
    __queueObject = ""

    def __init__(self, clientConnection, clientName, topicID, queueObject):
        self.__clientConnection = clientConnection
        self.__clientName = clientName
        self.__topicID = topicID
        self.__queueObject = queueObject

    def connectWithDatabase(self):
        self.__databaseController.initDatabaseConnection()

    def savePublisherTopicDataInTable(self):
        self.__databaseController.savePublisherTopicDataInTable(
            self.__clientName, self.__topicID)

    def publishMessages(self):
        try:
            while True:
                clientJsonMessageResponse = self.__communicationController.receiveMessageFromClient(
                    self.__clientConnection)
                if clientJsonMessageResponse[DATA].lower() == self.__EXIT:
                    break
                try:
                    messageObject = self.__getMessageObject(clientJsonMessageResponse[DATA])
                    self.__databaseController.saveDataInMessageTable(
                       messageObject, self.__topicID)
                    serialNumberOfMessage = self.__databaseController.getSerialNumberOfLastMessage()
                    messageObject.serialNo = serialNumberOfMessage
                    self.__publishMessageInQueue(messageObject)
                except Exception as e:
                    print(e)
                    exit()
                data = "server acknowledge"
                self.__communicationController.sendMessageToClient(
                    self.__clientConnection, data)

        except ConnectionResetError as error:
            print("Connection is closed")
        except Exception as error:
            print(error)

    def __getMessageObject(self, messageData):
        return Messages(messageData, self.__getCurrentTime(), self.__getExpiryTime())

    def __getCurrentTime(self):
        currentTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return currentTime

    def __getExpiryTime(self):
        expiryTime = (datetime.now()+ timedelta(hours = 12)).strftime('%Y-%m-%d %H:%M:%S')
        return expiryTime
    
    def __publishMessageInQueue(self, messageObject):
        isTopicExistInQueue = False

        for topic in self.__queueObject.data:
            if(topic.topicId == self.__topicID):
                isTopicExistInQueue = True
                topic.messages.append(messageObject)

        if len(self.__queueObject.data) == 0:
            topicObject = self.__getTopicObject(messageObject)
            self.__queueObject.data.append(topicObject)
            isTopicExistInQueue = True

        if not isTopicExistInQueue:
            topicObject = self.__getTopicObject(messageObject)
            self.__queueObject.data.append(topicObject)

        print(self.__queueObject.data)

    def __getTopicObject(self, messageObject):
        TopicObject = Topic()
        TopicObject.topicId = self.__topicID
        TopicObject.messages.append(messageObject)
        return TopicObject


