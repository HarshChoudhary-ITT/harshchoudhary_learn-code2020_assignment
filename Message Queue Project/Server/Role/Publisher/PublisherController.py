import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")

from Server.Role.Publisher.PublisherService import *

class PublisherController:

    def initiateService(self, clientConnection, clientName, topicID, queueObject):
        publisherService = PublisherService(clientConnection, clientName, topicID, queueObject)
        publisherService.connectWithDatabase()
        publisherService.savePublisherTopicDataInTable()
        publisherService.publishMessages()