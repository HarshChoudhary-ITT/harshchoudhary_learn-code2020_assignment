import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.CommunicationController import *
from Server.Database.DatabaseController import *
from Server.Model.Queue import *

class SubscriberService:

    __clientConnection = ""
    __clientName = ""
    __topicID = 0
    __databaseController = DatabaseController()
    __messageList = ""
    __communicationController = CommunicationController()
    __queueObject = ""

    def __init__(self, clientConnection, clientName, topicID, queueObject):
        self.__clientConnection = clientConnection
        self.__clientName = clientName
        self.__topicID = topicID
        self.__queueObject = queueObject

    def connectWithDatabase(self):
        self.__databaseController.initDatabaseConnection()

    def addDataInSubscriberTopicTable(self):
        self.__databaseController.saveSubscriberTopicDataInTable(
            self.__clientName, self.__topicID)

    def fetchDataFromMessageTable(self):
        serialNumberOfLastMessage: int
        if not self.__databaseController.isClientExistInSubscriberMessageMappingTable(self.__topicID, self.__clientName):
            self.__messageList = self.__getMessagesFromQueue()
            if self.__messageList:
                serialNumberOfLastMessage = self.__getSerialNumberofLastMessage()
                self.__databaseController.saveDataInSubscriberMessageMappingTable(
                    serialNumberOfLastMessage, self.__topicID, self.__clientName)
        else:
            serialNumberOfLastMessage = self.__databaseController.fetchMessageSerialNumberFromSubscriberMessageMappingTable(
                self.__topicID, self.__clientName)[0][0]
            self.__messageList = self.__getNewMessagesFromQueue(
                serialNumberOfLastMessage)
            if self.__messageList:
                serialNumberOfLastMessage = self.__getSerialNumberofLastMessage()
                self.__databaseController.updateMessageSerialNumberInSubscriberMessageMappingTable(
                    serialNumberOfLastMessage, self.__topicID, self.__clientName)

    def sendMessagesToClient(self):
        self.__communicationController.sendMessageToClient(
        self.__clientConnection, self.__messageList)

    def __getMessagesFromQueue(self):
        messageList = [tuple()]
        for topic in self.__queueObject.data:
            if(topic.topicId == self.__topicID):
                for message in topic.messages:
                    messageList.append(
                        (message.messageData, message.serialNo))
        if not messageList[0]:
            del messageList[0]
        return messageList

    def __getNewMessagesFromQueue(self, serialNumberOfLastMessage):
        messageList = [tuple()]
        for topic in self.__queueObject.data:
            if(topic.topicId == self.__topicID):
                for message in topic.messages:
                    if(message.serialNo > int(serialNumberOfLastMessage)):
                        messageList.append(
                            (message.messageData, message.serialNo))

        if not messageList[0]:
            del messageList[0]
        return messageList

    def __getSerialNumberofLastMessage(self):
        lenthOfMessageList = len(self.__messageList)
        serialNumber = self.__messageList[lenthOfMessageList-1][1]
        return serialNumber
