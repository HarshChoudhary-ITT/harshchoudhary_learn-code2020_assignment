import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")

from Server.Role.Subscriber.SubscriberService import *

class SubscriberController:

    def initiateService(self, clientConnection, clientName, topicID, queueObject):
        subscriberService = SubscriberService(clientConnection, clientName, topicID, queueObject)
        subscriberService.connectWithDatabase()
        subscriberService.addDataInSubscriberTopicTable()
        subscriberService.fetchDataFromMessageTable()
        subscriberService.sendMessagesToClient()

