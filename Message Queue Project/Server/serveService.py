import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.Role.Subscriber.SubscriberController import *
from Server.Role.Publisher.PublisherController import *
import socket
from _thread import *
from Server.CommunicationController import *
from Server.ServerConstant import *
from Server.Database.DatabaseController import *
from Server.Topic.TopicController import *
from Server.Scheduler.DeadLetterQueueScheduler import *
from Server.CustomExceptions.ServerListenerException import *
from Server.CustomExceptions.CreateSocketException import *
from Server.CustomExceptions.BindSocketException import *

class serverService:
    __availableTopics = []
    __communicationController = CommunicationController()
    __dbController = DatabaseController()
    __queueObject = ""

    def createSocket(self):
        try:
            serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            return serverSocket
        except:
            raise CreateSocketException(CREATE_SOCKET_ERROR_MESSAGE)
            exit()

    def bindSocket(self, serverSocket):
        try:
            serverSocket.bind((HOST, PORT))
        except socket.error:
            raise BindSocketException(BIND_SOCKET_ERROR_MESSAGE)
            exit()

    def setServerListener(self, serverSocket):
        try:
            serverSocket.listen(6)
        except:
            raise ServerListenerException(SERVER_LISTENER_EXCEPTION_MESSAGE)
            exit()

    def acceptConnectionWithClient(self, serverSocket):
        return serverSocket.accept()

    def createDatabaseTables(self):
        self.__dbController.initDatabaseConnection()
        self.__dbController.createDatabaseTables()

    def initQueue(self):
        self.__queueObject = Queue()

    def initDeadLetterQueueScheduler(self):
        scheduler = deadLetterScheduler()
        try:
            start_new_thread(scheduler.initScheduler, (self.__queueObject,))
        except DeadLetterQueueSchedulerException as error:
            errorMessage = error.args[0]
            print(errorMessage)
        except:
            print(DEAD_LETTER_NOT_WORKING_MESSAGE)

    def initializeTopic(self):
        topicController = TopicController()
        topicController.initializeService()
        self.__availableTopics = topicController.getTopicsList()

    def start_new_client_thread(self, clientConnection, clientName):
        self.saveClientDetailsInRootTable(clientName)
        self.sendWelcomeMessageToClient(clientConnection)
        try:
            while True:
                clientJsonRoleResponse = self.getRoleResponseFromClient(
                    clientConnection)
                self.sendAvailableTopicsToClient(clientConnection)
                clientJsonTopicResponse = self.getTopicResponseFromClient(
                    clientConnection)

                if(int(clientJsonRoleResponse[DATA]) == PUBLISHER):
                    publisherController = PublisherController()
                    publisherController.initiateService(
                        clientConnection, clientName, clientJsonTopicResponse[DATA], self.__queueObject)

                else:
                    subscriberController = SubscriberController()
                    subscriberController.initiateService(
                        clientConnection, clientName, clientJsonTopicResponse[DATA], self.__queueObject)
        except:
            clientConnection.close()
            print(CONNECTION_CLOSED_MESSAGE)

    def saveClientDetailsInRootTable(self, clientName):
        self.__dbController.initDatabaseConnection()
        self.__dbController.saveClientReferenceInRootTable(clientName)

    def sendWelcomeMessageToClient(self, clientConnection):
        self.__communicationController.sendMessageToClient(
            clientConnection, WELCOME_MESSAGE+"\n")

    def getRoleResponseFromClient(self, clientConnection):
        clientJsonRoleResponse = self.__communicationController.receiveMessageFromClient(
            clientConnection)
        print(CLIENT_ROLE_MESSAGE + clientJsonRoleResponse[DATA])
        return clientJsonRoleResponse

    def sendAvailableTopicsToClient(self, clientConnection):
        self.__communicationController.sendMessageToClient(
            clientConnection, self.__availableTopics)

    def getTopicResponseFromClient(self, clientConnection):
        clientJsonTopicResponse = self.__communicationController.receiveMessageFromClient(
            clientConnection)
        print(CLIENT_CHOOSE_TOPIC_MESSAGE + clientJsonTopicResponse[DATA])
        return clientJsonTopicResponse

    def closeSocket(self, serverSocket):
        serverSocket.close()
