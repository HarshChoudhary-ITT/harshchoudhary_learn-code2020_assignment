import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import Server.Database.DBConstant as DBConstant
import mysql.connector


class DbConnection:
    __instance = None

    @staticmethod
    def getInstance():
        if DbConnection.__instance == None:
            DbConnection()
        return DbConnection.__instance

    def __init__(self):
        if DbConnection.__instance != None:
            raise Exception("One Connection already Exists")
        else:
            myDb = mysql.connector.connect(
                host=DBConstant.DB_LOCALHOST,
                user=DBConstant.DB_USER_NAME,
                password=DBConstant.DB_USER_PASSWORD,
                database=DBConstant.DB_NAME
            )
            DbConnection.__instance = myDb


