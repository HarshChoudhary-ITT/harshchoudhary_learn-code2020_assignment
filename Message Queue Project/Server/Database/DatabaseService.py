import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.Database.DBConnector import *
from Server.Database.DatabaseQueries import *
import mysql.connector
from _datetime import datetime



class DatabaseService:
    __myDbObject = ""
    __cursor = ""
    __isTopicDataAdded = True

    def connectWithDatabase(self):
        self.__myDbObject = DbConnection.getInstance()
        self.__cursor = self.__getCursorObject(self.__myDbObject)

    def saveDataInMessageTable(self, messageObject, topicID):
        query = saveDataInClientTableCommand
        value = [messageObject.arrivalTime, messageObject.messageData, topicID]
        self.__cursor.execute(query, value)
        self.__myDbObject.commit()
        print(self.__cursor.rowcount, "record inserted in client table.")

    def getSerialNumberOfLastMessage(self):
        return self.__cursor.lastrowid

    def isClientTableExist(self):
        query = showTableCommand
        self.__cursor.execute(query)
        if(len(self.__cursor.fetchall()) > 0):
            return True
        else:
            return False

    def createClientMessageTable(self):
        query = createClientTableCommand
        self.__cursor.execute(query)

    def createRootTable(self):
        query = createRootTableCommand
        self.__cursor.execute(query)

    def createTopicTable(self):
        try:
            self.__cursor.execute(dropTopicTableCommand)
            query = createTopicTableCommand
            self.__cursor.execute(query)
        except:
            self.__isTopicDataAdded = False

    def createPublisherTopicTable(self):
        query = createPublisherTopicTableCommand
        self.__cursor.execute(query)

    def createSubscriberTopicTable(self):
        query = createSubscriberTopicTableCommand
        self.__cursor.execute(query)

    def createSubscriberMessageMappingTable(self):
        query = createSubscriberMessageMappingTableCommand
        self.__cursor.execute(query)

    def addTopicDataIntoTable(self, listOfTopics):
        if (self.__isTopicDataAdded):
            query = saveTopicDataCommand
            self.__cursor.executemany(query, listOfTopics)
            self.__myDbObject.commit()

    def savePublisherTopicDataInTable(self, clientName, topicID):
        clientID = self.getClientId(clientName)
        value = [clientID, topicID]
        self.__cursor.execute(isClientExistInPublisherTopicTableCommand, value)
        isClientExists = self.__cursor.fetchall()[0][1]
        if not isClientExists:
            query = savePublisherTopicDataCommand
            value = [topicID, clientID]
            self.__cursor.execute(query, value)
            self.__myDbObject.commit()
            print(self.__cursor.rowcount, "record inserted in publisher topic table")

    def saveSubscriberTopicDataInTable(self, clientName, topicID):
        clientID = self.getClientId(clientName)
        value = [clientID, topicID]
        self.__cursor.execute(isClientExistInSubscriberTopicTableCommand, value)
        isClientExists = self.__cursor.fetchall()[0][1]
        if not isClientExists:
            query = saveSubscriberTopicDataCommand
            value = [topicID, clientID]
            self.__cursor.execute(query, value)
            self.__myDbObject.commit()
            print(self.__cursor.rowcount,
                  "recorded inserted in subscriber topic table")

    def getClientId(self, clientName):
        value = [clientName, ]
        self.__cursor.execute(getClientIDCommand, value)
        clientID = self.__cursor.fetchall()[0][0]
        return clientID

    def saveClientReferenceInRootTable(self, clientName):
        value = [clientName, ]
        self.__cursor.execute(isClientExistInRootTableCommand, value)
        isClientExists = self.__cursor.fetchall()[0][1]
        if not isClientExists:
            query = saveDataInRootTableCommand
            value = [clientName]
            self.__cursor.execute(query, value)
            self.__myDbObject.commit()
            print(self.__cursor.rowcount, "record inserted in root table.")

    def saveDataInSubscriberMessageMappingTable(self, serialNumberOfLastMessage, topicID, clientName):
        clientId = self.getClientId(clientName)
        query = saveSubscriberMessageMappingTableCommand
        value = [serialNumberOfLastMessage, topicID, clientId]
        self.__cursor.execute(query, value)
        self.__myDbObject.commit()

    def isClientExistInSubscriberMessageMappingTable(self, topicId, clientName):
        clientID = self.getClientId(clientName)
        value = [topicId, clientID]
        query = isClientExistInSubscriberMessageMappingTableCommand
        self.__cursor.execute(query, value)
        isClientExists = self.__cursor.fetchall()[0][1]
        return isClientExists

    def fetchMessageSerialNumberFromSubscriberMessageMappingTable(self, topicID, clientName):
        clientID = self.getClientId(clientName)
        value = [topicID, clientID]
        query = fetchSerialNumberFromSubscriberMessageMappingTableCommand
        self.__cursor.execute(query, value)
        messageSerialNumber = self.__cursor.fetchall()
        return messageSerialNumber

    def updateMessageSerialNumberInSubscriberMessageMappingTable(self, serialNumberOfLastMessage, topicID, clientName):
        query = updateMessageSerialNumberInSubscriberMessageMappingTableCommand
        clientID = self.getClientId(clientName)
        value = [serialNumberOfLastMessage, topicID, clientID]
        self.__cursor.execute(query, value)
        self.__myDbObject.commit()
        print(self.__cursor.rowcount, "record(s) affected")

    def closeConnection(self):
        if (self.__myDbObject.is_connected()):
            self.__cursor.close()
            self.__myDbObject.close()
            print("MySQL connection is closed")


    def __getCursorObject(self, dbObject):
        cursorObject = dbObject.cursor()
        return cursorObject

   