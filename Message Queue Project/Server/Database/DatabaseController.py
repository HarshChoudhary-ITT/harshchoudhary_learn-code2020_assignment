import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Server.Database.DatabaseService import *

class DatabaseController:

    __dbService = DatabaseService()

    def initDatabaseConnection(self):
        self.__dbService.connectWithDatabase()

    def createDatabaseTables(self):
        self.__dbService.createRootTable()
        self.__dbService.createTopicTable()
        self.__dbService.createClientMessageTable()
        self.__dbService.createPublisherTopicTable()
        self.__dbService.createSubscriberTopicTable()
        self.__dbService.createSubscriberMessageMappingTable()

    def saveClientReferenceInRootTable(self, clientName):
        self.__dbService.saveClientReferenceInRootTable(clientName)

    def addTopicDataIntoTable(self, listOfTopics):
        self.__dbService.addTopicDataIntoTable(listOfTopics)

    def savePublisherTopicDataInTable(self, clientName, topicID):
        self.__dbService.savePublisherTopicDataInTable(clientName, topicID)

    def saveDataInMessageTable(self, message, topicID):
        self.__dbService.saveDataInMessageTable(message, topicID)

    def getSerialNumberOfLastMessage(self):
        return self.__dbService.getSerialNumberOfLastMessage()

    def saveSubscriberTopicDataInTable(self, clientName, topicID):
        self.__dbService.saveSubscriberTopicDataInTable(clientName, topicID)

    def isClientExistInSubscriberMessageMappingTable(self, topicId, clientName):
        isClientExists = self.__dbService.isClientExistInSubscriberMessageMappingTable(
            topicId, clientName)
        return isClientExists

    def saveDataInSubscriberMessageMappingTable(self, serialNumberOfLastMessage, topicID, clientName):
        self.__dbService.saveDataInSubscriberMessageMappingTable(
            serialNumberOfLastMessage, topicID, clientName)

    def fetchMessageSerialNumberFromSubscriberMessageMappingTable(self, topicID, clientName):
        return self.__dbService.fetchMessageSerialNumberFromSubscriberMessageMappingTable(topicID, clientName)

    def updateMessageSerialNumberInSubscriberMessageMappingTable(self, serialNumberOfLastMessage, topicID, clientName):
        self.__dbService.updateMessageSerialNumberInSubscriberMessageMappingTable(
            serialNumberOfLastMessage, topicID, clientName)
