createRootTableCommand = '''CREATE TABLE IF NOT EXISTS root_table
(
ID INT NOT NULL AUTO_INCREMENT,
client_name VARCHAR(40),
PRIMARY KEY(ID)
)'''

createClientTableCommand = '''CREATE TABLE IF NOT EXISTS client_message_table 
(
serial_no INT AUTO_INCREMENT PRIMARY KEY, 
timestamp VARCHAR(255), 
data VARCHAR(255), 
topic_ID INT,
FOREIGN KEY(Topic_ID) REFERENCES topics(topic_ID)
)'''

createTopicTableCommand = '''CREATE TABLE IF NOT EXISTS topics
(
topic_ID INT NOT NULL AUTO_INCREMENT, 
topic_name VARCHAR(50),
PRIMARY KEY(topic_ID)
)'''

createPublisherTopicTableCommand = '''CREATE TABLE IF NOT EXISTS publisher_topic_table
(
Serial_no INT NOT NULL AUTO_INCREMENT,
topic_ID INT NOT NULL,
client_ID INT NOT NULL,
FOREIGN KEY(client_ID) REFERENCES root_table(ID),
FOREIGN KEY(topic_ID) REFERENCES topics(topic_ID),
PRIMARY KEY(Serial_no)
)'''

createSubscriberTopicTableCommand = '''CREATE TABLE IF NOT EXISTS subscriber_topic_table
(
Serial_no INT NOT NULL AUTO_INCREMENT,
topic_ID INT NOT NULL,
client_ID INT NOT NULL,
FOREIGN KEY(client_ID) REFERENCES root_table(ID),
FOREIGN KEY(topic_ID) REFERENCES topics(topic_ID),
PRIMARY KEY(Serial_no)
)'''

createSubscriberMessageMappingTableCommand = ''' CREATE TABLE IF NOT EXISTS subscriber_message_mapping_table
(
message_serial_number INT NOT NULL,
topic_ID INT NOT NULL,
client_ID INT NOT NULL,
FOREIGN KEY(message_serial_number) REFERENCES client_message_table(serial_no),
FOREIGN KEY(client_ID) REFERENCES root_table(ID),
FOREIGN KEY(topic_ID) REFERENCES topics(topic_ID)
)'''

showTableCommand = """SHOW TABLES like 'client_message_table'"""

getClientIDCommand = '''SELECT ID FROM root_table WHERE client_name = (%s)'''

dropTopicTableCommand = '''DROP TABLE IF EXISTS topics'''

saveDataInClientTableCommand = "INSERT INTO client_message_table (serial_no,timestamp, data, topic_ID) VALUES (NULL,%s, %s, %s)"

saveDataInRootTableCommand = "INSERT INTO root_table (ID, client_name) VALUES (NULL, %s)"

saveTopicDataCommand = "INSERT INTO topics (topic_ID, topic_name) VALUES(NULL, %s)"

savePublisherTopicDataCommand = "INSERT INTO publisher_topic_table (serial_no, topic_ID, client_ID) VALUES(NULL,%s,%s) "

saveSubscriberTopicDataCommand = "INSERT INTO subscriber_topic_table (serial_no, topic_ID, client_ID) VALUES(NULL,%s,%s)"

saveSubscriberMessageMappingTableCommand = "INSERT INTO subscriber_message_mapping_table (message_serial_number,topic_ID, client_ID) VALUES(%s,%s,%s)"

fetchSerialNumberFromSubscriberMessageMappingTableCommand = "SELECT message_serial_number FROM subscriber_message_mapping_table WHERE topic_ID = (%s) AND client_ID = (%s)"

isClientExistInRootTableCommand = '''SELECT client_name,COUNT(*) FROM root_table WHERE client_name = (%s)'''

isClientExistInPublisherTopicTableCommand = '''SELECT client_ID, topic_ID, COUNT(*) from publisher_topic_table WHERE client_ID = (%s) AND topic_ID = (%s)'''

isClientExistInSubscriberTopicTableCommand = '''SELECT client_ID, topic_ID, COUNT(*) from subscriber_topic_table WHERE client_ID = (%s) AND topic_ID = (%s)'''

isClientExistInSubscriberMessageMappingTableCommand = '''SELECT client_ID, count(*) from subscriber_message_mapping_table WHERE topic_ID = (%s) AND client_ID = (%s)'''

updateMessageSerialNumberInSubscriberMessageMappingTableCommand = "UPDATE subscriber_message_mapping_table SET message_serial_number = (%s) WHERE topic_ID = (%s) AND client_ID = (%s)"
