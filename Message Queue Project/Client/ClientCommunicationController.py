import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Client.Protocol.Request import *
from Client.Protocol.Parser import *
from Client.ClientConstant import *

class ClientCommunicationController:
    def receiveMessageFromServer(self, clientSocket):
        response = clientSocket.recv(1024).decode('utf-8')
        responseJsonMessage = Parser().desrializeJsonFormattedData(response)
        return responseJsonMessage

    def sendMessageToServer(self, clientSocket, message):
        requestObject = Request(message)
        request = Parser().serializeToJsonConvertor(requestObject)
        clientSocket.send(str.encode(request))
