import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Client.ClientCommunicationController import *
from Client.ClientConstant import *
from Client.ClientService import *
from Client.CustomExceptions.SocketConnectionException import *

def clientStart():
    clientServiceInstance = ClientService()
    clientCommunicationController = ClientCommunicationController()
    clientSocket = clientServiceInstance.createSocket()
    try:
        clientServiceInstance.connectToServer(clientSocket)
        clientServiceInstance.receiveWelcomeMessageFromServer(clientSocket)
        while True:
            clientServiceInstance.initializeRole(clientSocket)
            clientServiceInstance.chooseTopicToConnect(clientSocket)
            if (clientServiceInstance.getClientRole() == PUBLISHER_ID):
                clientServiceInstance.communicationWithServerAsPublisher(
                    clientSocket)
            else:
                messageList = clientServiceInstance.communicationWithServerAsSubscriber(
                    clientSocket)
                clientServiceInstance.printMessagesOfSubscriber(messageList)

        clientServiceInstance.closeSocket(clientSocket)
    except SocketConnectionException as error:
        errorMessage = error.args[0]
        print(errorMessage)
    except ConnectToServerException as error:
        errorMessage = error.args[0]
        print(errorMessage)
    except:
        print(SOMETHING_WENT_WRONG_MESSAGE)


clientStart()
