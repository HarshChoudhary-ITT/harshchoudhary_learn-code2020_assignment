import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from dataclasses import *

@dataclass
class Role:
    roleID: list[int] = field(default_factory=lambda: [1,2])
    roleName: list[str] = field(default_factory=lambda: ['Publisher', 'Subscriber'])
