import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Client.Role.Role import Role
from Client.CommandParser import *

class RoleController :
    __roleObject = ""
    __commandParser = CommandParser()

    def initializeRole(self):
        self.__roleObject = Role()

    def takeInputFromUser(self):
        print("\n" + CHOOSE_ROLE_MESSAGE)
        print(CHOOSE_ROLE_COMMAND_MESSAGE)
        print("{ " + str(self.__roleObject.roleID[0]) +" : " +str(self.__roleObject.roleName[0])+" } "
                +"\n"+"{ " + str(self.__roleObject.roleID[1]) +" : " +str(self.__roleObject.roleName[1])+" } \n")
        while True:
            try:
                userInput = input()
                userRole = self.__commandParser.parseRoleCommand(userInput)
            except ParseRoleCommandException as error:
                errorMessage = error.args[0]
                print(errorMessage)
                continue
            except:
                print(PLEASE_ENTER_VALID_COMMAND)

            return userRole
            break



