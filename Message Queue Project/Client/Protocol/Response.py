from dataclasses import dataclass
from Client.Protocol.IMQProtocol import *
from Client.ClientConstant import *

@dataclass
class Response(IMQProtocol):
    status: str = MESSAGE_SENT