from dataclasses import dataclass
from Client.Protocol.IMQProtocol import *
from Client.ClientConstant import *

@dataclass
class Request(IMQProtocol):
    requestType: str = POST
