HOST = '127.0.0.1'
PORT = 1234
EXIT = "exit"

PUBLISHER_ID = 1
SUBSCRIBER_ID = 2
MESSAGE_SENT = "Message sent"
POST = "POST"

IMQ = "imq"
CONNECT = "connect"
USER = "user"
PUBLISHER = "publisher"
SUBSCRIBER = "subscriber"
PUBLISH = "publish"
PUBLISH_FLAG = "-m"

PLEASE_ENTER_VALID_COMMAND = "Please enter valid command..."
STATUS = "status"
DATA = "data"
SOCKET_SUCCESSFULLY_CREATED_MESSAGE = "Client Socket successfully created"
SOCKET_FAILED_TO_CREATE_MESSAGE = "socket creation failed with error %s"
CHOOSE_TOPIC_MESSAGE = "Please choose the Topic using command"
CHOOSE_TOPIC_COMMAND_MESSAGE = "Command :- imq connect topic_name"
COLON = " : "
PUBLISH_MESSAGE_USING_COMMAND = "Publish message using command"
PUBLISH_MESSAGE_COMMAND_MESSAGE = "Command :- imq publish -m message_content"
EXIT_COMMAND_MESSAGE = "For exit use command :- imq exit"
SAY_SOMETHING_MESSAGE = "Say Something"
READ_ALL_MESSAGES = "No messages available to read."
WAITING_FOR_CONNECTION_MESSAGE = "waiting for connection with server"
SOMETHING_WENT_WRONG_MESSAGE = "Oops!, something went wrong, please try again"
CHOOSE_ROLE_MESSAGE = "Please choose the role using command"
CHOOSE_ROLE_COMMAND_MESSAGE = "Command :- imq user role_name"
SERVER_IS_DOWN_ERROR = "Server is down, not able to connect it."