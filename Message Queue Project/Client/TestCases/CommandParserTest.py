import sys
sys.path.append("D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import unittest
from unittest.mock import patch
from Client.CommandParser import *

class CommandParser_Test(unittest.TestCase):
    commandParser = CommandParser()

    def testParseRoleCommandAsPublisher(self):
        publisherConnectCommand = "imq user publisher"
        userRole = self.commandParser.parseRoleCommand(publisherConnectCommand)
        self.assertEqual(userRole, 1)

    def testParseRoleCommandAsSubscriber(self):
        subscriberConnectCommand = "imq user subscriber"
        userRole = self.commandParser.parseRoleCommand(subscriberConnectCommand)
        self.assertEqual(userRole, 2)

    def testChooseTopicData(self):
        topicList = [['Morning Updates'], ['Evening Updates']]
        topicConnectCommand = "imq connect morning updates"
        topicId = self.commandParser.parseChooseTopicDataCommand(topicList, topicConnectCommand)
        self.assertEqual(topicId, 1)

    def testParsePublishMessage(self):
        publishMessageCommand = "imq publish -m good morning all"
        message = self.commandParser.parsePublisherMessageCommand(publishMessageCommand)
        self.assertEqual(message, "good morning all")
