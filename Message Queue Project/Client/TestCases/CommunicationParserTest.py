import sys
sys.path.append("D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import unittest
from unittest.mock import patch
from Client.Protocol.Parser import *
from Client.Protocol.Request import *
from Client.ClientConstant import *

class CommunicationParser_Test(unittest.TestCase):
    __parserObject = Parser()

    def testIsInstanceOfParser(self):
        self.assertIsInstance(self.__parserObject, Parser)

    def testSerializeToJsonConvertor(self):
        message = "message"
        requestObject = Request(message)
        response = self.__parserObject.serializeToJsonConvertor(requestObject)
        self.assertEqual(response, '"{\\"data\\": \\"message\\", \\"dataFormat\\": \\"json\\", \\"version\\": \\"1.0\\", \\"requestType\\": \\"POST\\"}"')
    
    def testDeserializeJsonFormatedData(self):
        message = "message"
        requestObject = Request(message)
        serializeResponse = self.__parserObject.serializeToJsonConvertor(requestObject)
        desrializeResponse = self.__parserObject.desrializeJsonFormattedData(serializeResponse)
        self.assertEqual(desrializeResponse, {'data': 'message', 'dataFormat': 'json', 'version': '1.0', 'requestType': 'POST'})
