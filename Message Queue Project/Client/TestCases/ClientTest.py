import sys
sys.path.append("D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
import unittest
from unittest.mock import patch
import socket
from Client.ClientService import ClientService

class Client_test(unittest.TestCase):
    __ClientObject = ClientService()
    ClientSocket = socket.socket()

    def testIsIstanceOfClientService(self):
        self.assertIsInstance(self.__ClientObject, ClientService)

    def testClientSocketcreation(self):
        socketCreated = self.__ClientObject.createSocket()
        self.assertEqual(type(socketCreated), type(self.ClientSocket))
        socketCreated.close()

    def testConnectionWithServer(self):
        self.assertIsNone(self.__ClientObject.connectToServer(self.ClientSocket))
    






