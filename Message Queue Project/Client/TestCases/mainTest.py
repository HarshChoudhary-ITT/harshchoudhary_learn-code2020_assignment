import os
import sys
path = os.path.abspath(__file__)
path = path + "/../../.."
sys.path.append(path)

from Client.TestCases.ClientTest import *
from Client.TestCases.CommandParserTest import *
from Client.TestCases.CommunicationParserTest import *

if __name__ == '__main__':  
    unittest.main()