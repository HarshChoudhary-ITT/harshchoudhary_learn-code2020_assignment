import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Client.ClientCommunicationController import *
import socket
from Client.ClientConstant import *
from Client.CommandParser import *
from Client.Role.RoleController import *
from Client.CustomExceptions.SocketConnectionException import *
from Client.CustomExceptions.ConnectToServerException import *

class ClientService:
    __clientRole: int
    __clientCommunicationController = ClientCommunicationController()
    __commandParser = CommandParser()

    def getClientRole(self):
        return self.__clientRole

    def createSocket(self):
        try:
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as err:
            raise SocketConnectionException(SOCKET_FAILED_TO_CREATE_MESSAGE % (err))
        return clientSocket

    def connectToServer(self, clientSocket):
        try:
            clientSocket.connect((HOST, PORT))
        except:
            raise ConnectToServerException(SERVER_IS_DOWN_ERROR)
            exit()

    def receiveWelcomeMessageFromServer(self, clientSocket):
        jsonWelcomeResponse = self.__clientCommunicationController.receiveMessageFromServer(
            clientSocket)
        print(jsonWelcomeResponse[DATA])

    def initializeRole(self, clientSocket):
        roleController = RoleController()
        roleController.initializeRole()
        self.takeUserInputForRole(roleController, clientSocket)

    def takeUserInputForRole(self, roleController, clienSocket):
        self.__clientRole = roleController.takeInputFromUser()
        self.__clientCommunicationController.sendMessageToServer(
            clienSocket, str(self.__clientRole))

    def chooseTopicToConnect(self, clientSocket):
        jsonTopicResponse = self.__clientCommunicationController.receiveMessageFromServer(
            clientSocket)
        topicList = jsonTopicResponse[DATA]
        print("\n" + CHOOSE_TOPIC_MESSAGE)
        print(CHOOSE_TOPIC_COMMAND_MESSAGE)
        for index in range(len(topicList)):
            print("{ " + str(index+1) + COLON +
                  str(topicList[index][0]) + " }")
        while True:
            try:
                inputCommand = input("\n")
                topicId = self.__commandParser.parseChooseTopicDataCommand(
                    topicList, inputCommand)
            except ParseTopicDataCommandException as error:
                errorMessage = error.args[0]
                print(errorMessage)
                continue
            except Exception as e:
                print(PLEASE_ENTER_VALID_COMMAND)
                continue
            break
        self.__clientCommunicationController.sendMessageToServer(
            clientSocket, str(topicId))

    def communicationWithServerAsPublisher(self, clientSocket):
        print("\n" + PUBLISH_MESSAGE_USING_COMMAND)
        print(PUBLISH_MESSAGE_COMMAND_MESSAGE)
        print(EXIT_COMMAND_MESSAGE)
        while True:
            while True:
                try:
                    inputCommand = input("\n" + SAY_SOMETHING_MESSAGE + "\n")
                    message = self.__commandParser.parsePublisherMessageCommand(
                        inputCommand)
                except ParsePublishMessageCommandException as error:
                    errorMessage = error.args[0]
                    print(errorMessage)
                    continue
                except Exception as error:
                    print(PLEASE_ENTER_VALID_COMMAND)
                    continue
                break
            self.__clientCommunicationController.sendMessageToServer(
                clientSocket, message)
            if message.lower() == EXIT:
                break
            responseJsonMessage = self.__clientCommunicationController.receiveMessageFromServer(
                clientSocket)
            print(responseJsonMessage[STATUS])

    def communicationWithServerAsSubscriber(self, clientSocket):
        jsonMessageResponse = self.__clientCommunicationController.receiveMessageFromServer(
            clientSocket)
        return jsonMessageResponse[DATA]

    def printMessagesOfSubscriber(self, messageList):
        if not messageList:
            print(READ_ALL_MESSAGES)
        else:
            for index in range(len(messageList)):
                print(messageList[index][0])

    def closeSocket(self, clientSocket):
        clientSocket.close()
