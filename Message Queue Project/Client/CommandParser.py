import sys
sys.path.append(
    "D:\harshchoudhary_learn-code2020_assignment\Message Queue Project")
from Client.ClientConstant import *
from Client.CustomExceptions.ParseRoleCommandException import *
from Client.CustomExceptions.ParseTopicDataCommandException import *
from Client.CustomExceptions.ParsePublishMessageCommandException import *

class CommandParser:

    def parseRoleCommand(self, userInput):
        userRoleCommandList = userInput.split(" ")
        if(len(userRoleCommandList) == 3 and userRoleCommandList[0].lower() == IMQ and userRoleCommandList[1].lower() == USER and userRoleCommandList[2].lower() == PUBLISHER):
            return PUBLISHER_ID
        elif(len(userRoleCommandList) == 3 and userRoleCommandList[0].lower() == IMQ and userRoleCommandList[1].lower() == USER and userRoleCommandList[2].lower() == SUBSCRIBER):
            return SUBSCRIBER_ID
        else:
            raise ParseRoleCommandException(PLEASE_ENTER_VALID_COMMAND)

    def parseChooseTopicDataCommand(self, topicList, chooseTopicCommand):
        chooseTopicCommandList = chooseTopicCommand.split(" ")
        choosenTopicId = 0
        if (chooseTopicCommandList[0].lower() == IMQ and chooseTopicCommandList[1].lower() == CONNECT):
            topicData = self.__getTopicData(chooseTopicCommandList)
            for index in range(len(topicList)):
                if (topicData.lower() == str(topicList[index][0]).lower()):
                    chooseTopicId = index+1
            if chooseTopicId == 0:
                raise ParseTopicDataCommandException(
                    PLEASE_ENTER_VALID_COMMAND)
        else:
            raise ParseTopicDataCommandException(PLEASE_ENTER_VALID_COMMAND)
        return chooseTopicId

    def parsePublisherMessageCommand(self, publishMessageCommand):
        messageCommandList = publishMessageCommand.split(" ")
        if(messageCommandList[0].lower() == IMQ and messageCommandList[1].lower() == PUBLISH and messageCommandList[2].lower() == PUBLISH_FLAG):
            messageData = self.__getMessageData(messageCommandList)
        elif(len(messageCommandList) == 2 and messageCommandList[0].lower() == IMQ and messageCommandList[1].lower() == EXIT):
            messageData = messageCommandList[1].lower()
        else:
            raise ParsePublishMessageCommandException(
                PLEASE_ENTER_VALID_COMMAND)
        return messageData

    def __getMessageData(self, messageCommandList):
        messageData = ""
        messageDataList = messageCommandList[3:]
        for index in range(len(messageDataList)):
            if index+1 != len(messageDataList):
                messageData = messageData + messageDataList[index] + " "
            else:
                messageData = messageData + messageDataList[index]
        return messageData

    def __getTopicData(self, chooseTopicCommand):
        topicData = ""
        topicDataList = chooseTopicCommand[2:]
        for index in range(len(topicDataList)):
            if index+1 != len(topicDataList):
                topicData = topicData + topicDataList[index] + " "
            else:
                topicData = topicData + topicDataList[index]
        return topicData
