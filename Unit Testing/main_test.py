import unittest
import main
from unittest.mock import patch

class Test(unittest.TestCase):
  
    def test_getSameNumberOfDivisor(self):
        result = main.getSameNumberOfDivisor(15)
        self.assertEqual(result, 2)

    def test_getSameNumberOfDivisorForTwo(self):
        result = main.getSameNumberOfDivisor(2)
        self.assertEqual(result, 0)

    @patch('main.printPairs')
    def test_printPairs_is_called(self, mock):
        main.getSameNumberOfDivisor(100)
        self.assertTrue(mock)

    
if __name__ == '__main__':
    unittest.main()