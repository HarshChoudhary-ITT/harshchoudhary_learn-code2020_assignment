def getSameNumberOfDivisor(inputNumber):
     countPairs = 0            
     for number in range(2,inputNumber):
        firstNumberFactorList = []
        secondNumberFactorList = []
            
        for divisor in range(1,inputNumber+1):
            if number%divisor == 0:
                firstNumberFactorList.append(divisor)
            if (number+1)%divisor == 0:
                secondNumberFactorList.append(divisor)

                
        if len(firstNumberFactorList) == len(secondNumberFactorList):
            countPairs += 1
     return countPairs

def printPairs(resultList):
    for index in range(len(resultList)):
        print(resultList[index])


def main():
    resultList = []
    testcases = int(input())
    while testcases > 0:
        inputNumber = int(input())
        if(inputNumber < 1):
            print("Please enter correct Value..!!")
            exit()
        resultList.append(getSameNumberOfDivisor(inputNumber))
        testcases -= 1
    
    printPairs(resultList)

main()