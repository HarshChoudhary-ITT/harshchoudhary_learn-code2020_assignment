import atmException.ServerException
import java.lang.Exception
import java.net.ServerSocket


fun main(args: Array<String>) {
    try {
        BankServerInitialization().initializeServer()
    } catch (exception: ServerException) {
        println(exception.message)
    }
}


class BankServerInitialization {
    fun initializeServer() {
        val server = ServerSocket(8189)
        try {
            while (true) {
                server.accept()
                println("Bank Server is running")
            }
        } catch (exception: Exception) {
            throw ServerException("Server is not running")
        }
    }
}