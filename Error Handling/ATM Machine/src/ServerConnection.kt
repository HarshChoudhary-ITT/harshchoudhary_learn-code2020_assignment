import atmException.ServerConnectionException
import java.lang.Exception
import java.net.Socket

class ServerConnection {
    fun connectToServer() {
        try {
            Socket("localhost", 8189)
            println("Connection Establish with Bank Server")
        } catch (exception: Exception) {
            throw ServerConnectionException("Unable to Connect with Server")
        }
    }
}