package atmException

import java.lang.Exception

class CardBlockException(message: String) : Exception(message)