package atmException

import java.lang.Exception

class InsufficientAmountException(message: String) : Exception(message)