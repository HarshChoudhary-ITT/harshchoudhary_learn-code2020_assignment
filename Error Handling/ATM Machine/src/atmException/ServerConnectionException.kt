package atmException

import java.lang.Exception

class ServerConnectionException (message:String):Exception(message)