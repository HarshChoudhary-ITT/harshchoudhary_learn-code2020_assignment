package atmException

import java.lang.Exception

class InvalidPinException(message:String):Exception(message)