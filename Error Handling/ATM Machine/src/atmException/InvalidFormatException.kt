package atmException

import java.lang.Exception

class InvalidFormatException(message:String):Exception(message)