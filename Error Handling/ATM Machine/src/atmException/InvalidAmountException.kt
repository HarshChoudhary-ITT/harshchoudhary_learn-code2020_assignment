package atmException

import java.lang.Exception

class InvalidAmountException(message:String):Exception(message)