package atmException

import java.lang.Exception

class OutOfCashException(message:String):Exception(message)