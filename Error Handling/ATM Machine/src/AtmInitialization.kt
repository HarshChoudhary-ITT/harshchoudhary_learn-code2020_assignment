import atmException.CardBlockException
import atmException.InsufficientAmountException
import atmException.InvalidFormatException

class AtmInitialization {

    var atmServices = AtmServices()

    fun init() {
        println(
            "Welcome to InTimeTec ATM\n" +
                    "Please enter your pin Number\n"
        )

        try {
            atmServices.checkPinNumber()
            atmServices.withdrawAmount()
        } catch (exception: CardBlockException) {
            println(exception.message)
        } catch (exception: InsufficientAmountException) {
            println(exception.message)
        } catch (exception: InvalidFormatException) {
            println(exception.message)
        }
    }

}