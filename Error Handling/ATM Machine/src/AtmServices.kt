import atmException.*
import java.lang.Exception

class AtmServices {
    private var numberOfAttempts = 0
    private var pinNumber = 0
    private var debitAmount = 0
    private var amountInAtm = 10000
    private var isPinNumberCorrect = false

    fun checkPinNumber() {
        checkPinNumberFormat()

        while (numberOfAttempts != 3) {
            try {
                when (CustomerAccount.pinNumber) {
                    this.pinNumber -> {
                        isPinNumberCorrect = true
                        return
                    }
                    else -> {
                        numberOfAttempts += 1
                        if (numberOfAttempts < 3)
                            throw InvalidPinException("Invalid Pin, Please try Again")
                    }
                }
            } catch (exception: InvalidPinException) {
                println(exception.message)
                println("Please re-enter your pin Number")
                this.pinNumber = readLine()!!.toInt()
            }

        }

        if (numberOfAttempts == 3 && !isPinNumberCorrect) {
            throw CardBlockException("Max Limit Exceed, your card has been blocked")
        }
    }

    private fun checkPinNumberFormat() {
        try {
            pinNumber = readLine()!!.toInt()
        } catch (exception: Exception) {
            throw InvalidFormatException("Please enter numerical data")
        }
    }

    fun withdrawAmount() {
        println("Please enter the amount")
        checkWithdrawAmountFormat()

        try {
            when {
                debitAmount > amountInAtm -> {
                    throw OutOfCashException("ATM out of cash")
                }
                debitAmount > CustomerAccount.totalAmount -> {
                    throw InsufficientAmountException("Insufficient Amount in your Account")
                }
                else -> {
                    debitAmountValidator()
                    println(
                        "Transaction Successful..!!\n" +
                                "${CustomerAccount.name} amount left in your account is ${CustomerAccount.totalAmount}"
                    )
                }
            }
        } catch (exception: OutOfCashException) {
            println(exception.message)
        } catch (exception: InvalidAmountException) {
            println(exception.message)
        }
    }

    private fun debitAmountValidator() {
        if (debitAmount % 100 == 0) {
            CustomerAccount.totalAmount -= debitAmount
        } else {
            throw InvalidAmountException("Debit Amount should be in proper Format like 1000, 15000, 2700")
        }
    }

    private fun checkWithdrawAmountFormat() {
        try {
            debitAmount = readLine()!!.toInt()
        } catch (exception: Exception) {
            throw InvalidFormatException("Please enter numerical data")
        }
    }
}
