import atmException.ServerConnectionException

fun main() {
    try {
        ServerConnection().connectToServer()
        AtmInitialization().init()
    } catch (exception: ServerConnectionException) {
        println(exception.message)
    }

}
