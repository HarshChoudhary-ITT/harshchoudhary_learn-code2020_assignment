package storage.management;

import java.util.Scanner;

public class Main {
	private IStorage iStorage;
	private int inputNumber;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Main main = new Main();
		System.out.println("Welcome to Storage Management System");
		try {
			while (true) {
				init();
				main.getUserInput(scanner);
				main.selectStorageType();
				initOpertaions();
				main.getUserInput(scanner);
				main.performOperation();
			}
		} catch (InvalidNumberException exception) {
			System.out.println(exception.getMessage());
		} catch (TerminateProgramException exception) {
			System.out.println("Thanks for using Storage Management System");
		}
	}

	private void performOperation() {
		switch (inputNumber) {
		case 1:
			iStorage.saveDetails();
			break;
		case 2:
			iStorage.showDetails();
			break;
		default:
			throw new InvalidNumberException("Please choose valid number either 1 or 2..!!");
		}

	}

	private void selectStorageType() {
		switch (inputNumber) {
		case 1:
			iStorage = new Database();
			break;
		case 2:
			iStorage = new FileStorage();
			break;
		case 3:
			throw new TerminateProgramException();
		default:
			throw new InvalidNumberException("Please choose valid number either 1 or 2 or 3..!!");
		}
	}

	private void getUserInput(Scanner scanner) {
		try {
			inputNumber = scanner.nextInt();
		} catch (Exception exception) {
			throw new InvalidNumberException("Please choose valid number..!!");
		}
	}

	private static void initOpertaions() {
		System.out.println("Press 1 to Add details" + "\n" + "Press 2 to Show details");
	}

	private static void init() {
		System.out.println("Please select the following options in which you want to store the data");
		System.out.println("Press 1 to store data in Database." + "\n" + "Press 2 to store data in File System." + "\n"
				+ "Press 3 to exit.");
	}

}
