package storage.management;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class FileStorage implements IStorage {
	private static JSONArray studentList = new JSONArray();

	@SuppressWarnings("unchecked")
	public void saveDetails() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Name : ");
		String name = scanner.nextLine();
		System.out.println("Enter Email : ");
		String email = scanner.nextLine();
		System.out.println("Enter College Id Number : ");
		int collegeIdNumber = scanner.nextInt();
		System.out.println("Enter College Name : ");
		scanner.nextLine();
		String collegeName = scanner.nextLine();

		JSONObject studentDetails = new JSONObject();
		studentDetails.put("Name", name);
		studentDetails.put("Email", email);
		studentDetails.put("College Id Number", collegeIdNumber);
		studentDetails.put("College Name", collegeName);

		JSONObject studentObject = new JSONObject();
		studentObject.put("student", studentDetails);

		studentList.add(studentObject);

		try (FileWriter file = new FileWriter("student.json")) {
			file.write(studentList.toJSONString());
			file.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void showDetails() {
		JSONParser jsonParser = new JSONParser();

		try (FileReader reader = new FileReader("student.json")) {
			// Read JSON file
			Object obj = null;
			try {
				obj = jsonParser.parse(reader);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			JSONArray studentList = (JSONArray) obj;

			// Iterate over student array
			studentList.forEach(student -> parseStudentObject((JSONObject) student));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void parseStudentObject(JSONObject student) {
		JSONObject employeeObject = (JSONObject) student.get("student");

		String name = (String) employeeObject.get("Name");
		System.out.println(name);

		String email = (String) employeeObject.get("Email");
		System.out.println(email);

		long collegeIdNumber = (long) employeeObject.get("College Id Number");
		System.out.println(collegeIdNumber);

		String collegeNumber = (String) employeeObject.get("College Name");
		System.out.println(collegeNumber);

	}

}
