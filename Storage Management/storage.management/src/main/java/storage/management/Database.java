package storage.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Database implements IStorage {
	private static List<Student> studentList = new ArrayList<Student>();

	public void saveDetails() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Name : ");
		String name = scanner.nextLine();
		System.out.println("Enter Email : ");
		String email = scanner.nextLine();
		System.out.println("Enter College Id Number : ");
		int collegeIdNumber = scanner.nextInt();
		System.out.println("Enter College Name : ");
		scanner.nextLine();
		String collegeName = scanner.nextLine();
		Student student = new Student(name, email, collegeIdNumber, collegeName);
		studentList.add(student);

		System.out.println("Data has been Added Successfully..!!");
	}

	public void showDetails() {
		for (int index = 0; index < studentList.size(); index++) {
			Student student = studentList.get(index);
			System.out.println("Name : " + student.getName() + "\n" + "Email : " + student.getEmail() + "\n"
					+ "College Id Number : " + student.getNumber() + "\n" + "College Name : " + student.getCollege()
					+ "\n");

		}
	}

}
