package storage.management;

public class Student {
	private String name;
	private String email;
	private int collegeIdNumber;
	private String college;

	public Student(String name, String email, int collegeIdNumber, String college) {
		super();
		this.name = name;
		this.email = email;
		this.collegeIdNumber = collegeIdNumber;
		this.college = college;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNumber() {
		return collegeIdNumber;
	}

	public void setNumber(int number) {
		this.collegeIdNumber = number;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

}
