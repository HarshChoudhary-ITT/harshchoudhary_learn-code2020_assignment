import requests
import json

def checkPostRangeFormat(postRange):
    if len(postRange)==2 :
        return True
    else:
       return False

def initializeEndPoints(postRange):
    endpoints=[]
    if int(postRange[1])-int(postRange[0])>50:
        endpoints.append(postRange[0])
        endpoints.append(int(endpoints[0])+50)
    else:
        endpoints=postRange
    return endpoints
    
# return URl in format of https://good.tumblr.com/api/read/json?type=photo&num=50&start=0
def getUrl(blogName,start,end):
    return "https://"+blogName+".tumblr.com/api/read/json?type=photo&num="+str(end)+"&start="+str(start)

def getResponse(URL):
    return requests.get(URL)

def getJsonObject(response):
    return json.loads(response.text.split('var tumblr_api_read = ')[1][:-2])


def displayBlogInformation(jsonObject,isDiplayBlogInfo):
    if isDiplayBlogInfo==True:
        print('Title : '+str(jsonObject['tumblelog']['title'])+"\nName : "+str(jsonObject['tumblelog']['name'])+"\nDescription : "+str(jsonObject['tumblelog']['description'])+"\nNo. of post : "+str(jsonObject['posts-total'])+"\n")
        return False
        

def displayNumberOfPost(jsonObject,start,end):
    posts=jsonObject['posts']
    postNumber=start
    for post in posts:
        print(str(postNumber)+".",post['photo-url-1280'])
        postNumber+=1
        postPhotos=post['photos']
        for postphoto in postPhotos:
            #To avoid duplicasy, this condition is required
            if post['photo-url-1280']!=postphoto['photo-url-1280']:
                print("  ",postphoto['photo-url-1280'])  
        if(postNumber==end+1):
            break
                

# main business logic written here.   
def getPostsFromTumblrApiAndDisplay():
    blogName=input("enter the Tumblr Blog Name :- ")
    postRange=input("Enter the post Range :- ").split("-")
    isDiplayBlogInfo=True

    if checkPostRangeFormat(postRange):
        endpoints=initializeEndPoints(postRange)
        start=int(endpoints[0])
        end=int(endpoints[1])
    else:
        print("Range should be in proper format like (start-end)..!!")
        exit()
    

    while start != int(postRange[1]):
        URL=getUrl(blogName,start,end)
        response=getResponse(URL)

        try:
             jsonObject=getJsonObject(response)
        except:
             print("Please enter correct Blog Name..!!")
             exit()

        isDiplayBlogInfo=displayBlogInformation(jsonObject,isDiplayBlogInfo)
        displayNumberOfPost(jsonObject,start,end)

        start=end
        diff=int(postRange[1])-start
        if(diff>50):
            end=start+50
        else:
            end=end+diff
                   
getPostsFromTumblrApiAndDisplay()