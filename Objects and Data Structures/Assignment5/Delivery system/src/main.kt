fun main() {

    val billAmount = 40.0f
    val myCustomer = Customer("Harsh", "Choudhary")
    val deliveryBoy = DeliveryService()
    deliveryBoy.getPayment(myCustomer, billAmount)
}