class Customer(var firstName: String, var lastname: String) {

    private val myWallet = Wallet()

    fun payBill(billAmount: Float): Boolean {
        if (myWallet.totalMoney > billAmount) {
            myWallet.subtractMoney(billAmount)
            return true
        }
        return false
    }
}