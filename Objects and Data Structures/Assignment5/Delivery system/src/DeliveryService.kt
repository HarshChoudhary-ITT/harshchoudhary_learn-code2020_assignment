class DeliveryService {
    fun getPayment(myCustomer: Customer, billAmount: Float) {
        if (myCustomer.payBill(billAmount)) {
            print("Bill has been paid successfully")
        } else {
            print("Insufficient Amount, Please check the balance..!! ")
        }
    }
}