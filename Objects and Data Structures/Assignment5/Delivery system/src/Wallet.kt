class Wallet {
    var totalMoney = 1000f

    fun addMoney(deposit: Float) {
        totalMoney += deposit
    }

    fun subtractMoney(debit: Float) {
        totalMoney -= debit
    }

}