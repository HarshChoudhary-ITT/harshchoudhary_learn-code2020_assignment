import java.lang.Exception
import kotlin.random.Random

fun main(args: Array<String>) {
    while (true) {
        try {
            Thread.sleep(Random.nextLong(1000, 5000))
            Animal().getFood()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }
}