class Animal {
    var isHungry = true

    fun getFood() {
        isHungry = true
        Master().giveFood()
        eatFood()
    }

    private fun eatFood() {
        if (isHungry) {
            print("Had Food\n")
            isHungry = false
        }
    }

}